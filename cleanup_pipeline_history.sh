#!/bin/bash
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

set -e

TOKEN="$ACCESS_TOKEN_CI_VAR"
SKIP="$CLEANUP_PIPELINES_FROM_POSITION_CI_VAR"
PROJECT_ID="$CI_PROJECT_ID"
GITLAB_INSTANCE="https://gitlab.opencode.de/api/v4/projects"

for PIPELINE in $(curl --header "PRIVATE-TOKEN: $TOKEN" "$GITLAB_INSTANCE/$PROJECT_ID/pipelines?per_page=100" | sort -r | jq '.[].id' | tail -n "+$SKIP") ; do
    echo "Deleting pipeline $PIPELINE"
    curl --header "PRIVATE-TOKEN: $TOKEN" --request "DELETE" "$GITLAB_INSTANCE/$PROJECT_ID/pipelines/$PIPELINE"
done
