<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
**Content / Quick navigation**

[[_TOC_]]

# Public Git repository to Gitlab pull mirror automation

A fully automated solution for Gitlab Free tier to mirrors code from public git repositories.

# Files

The following sub sections describes the contents of the repository beside this `README.md` and the mandatory `LICENSE` information

## `.gitlab-ci.yml`

The CI prepares an Alpine container and executes the script `mirror_automation.py`

## `mirror_automation.py`

The script that does all the magic (creating/updating Gitlab groups and project, mirroring the configured source repositories into the projects).

### Execution

#### Input parameters

To execute the script you need to provide the environment variable `GITLAB_ACCESS_TOKEN` containing an access token that has owner priviledges into the target group structure you are creating the mirrors within.

#### Hard coded values

Some additional parameters are currently hard coded that might be interesting to modify or externalize later on:

- `gitlab_hostname`: The hostname of the target Gitlab repos are mirrored to.
- `git_username`: The name of the user that commits into a repository (only used in cases the script has to do a commit).
- `git_useremail`: The email of the user that commits into a repository (only used in cases the script has to do a commit).
- `global_visibility`: Defines the Gitlab visibility level for all managed groups and projects (e.g. `private`, `public`)
- Project configuration: The configuration projects are created/updated with is hardcoded. Find it near `# Hardcoded project configuration` in the code.

#### Config files

The following two config files are expected to exist on the script's startup.

##### `mirror_automation_main.json`

See [the file itself](mirror_automation_main.json) for reference.

The configuration just provides information to the script where to find all the entity specific configuration files which contain the actual information about the repositories to mirror.

In these files you can configure beside the mirror source and target also additional data to specify your mirror setup. Some more explanation below, but see also [one of the existing configuration files](./mirror-definitions/) for reference.

```
    "default_display_and_uri_name": {
        "source_url": "https://github_or_whatever_public_source.com/your/source/repository.git",
        "target_group": "path/to/the/group/the/source/git-repo/should/be/mirrored/below",
        "name": "Optional: Gitlab display name of the target project",
        "branch": "optional-source-branch",
        "extensions": {
            "delete_dir": ["array", "of", "directories", "from", "the", "source", "repo", "to", "remove", "before", "pushing", "to", "target"],
            "flatten_repo": ["Commit message to use when flattening the repo, ideally explains why flattening was choosen."]
        }
    },
```

##### `mirror_automation_groups.json`

See [the file itself](mirror_automation_groups.json) for reference.

This file just defined some metadata for groups that are probably being used in the mirror configurations.

## Trouble shooting

This section will be added one trouble by another.
