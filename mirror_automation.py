#!/usr/bin/python3
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import gitlab
import os
import sys
import shutil
import json
import logging
import subprocess

from git import Repo
from cryptography.hazmat.primitives import serialization as crypto_serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend as crypto_default_backend

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.NOTSET)
logging.info("Initializing basic parameters")

# Set the required variables, we might want to parametrize these in the future
gitlab_access_token_envvar_name = 'GITLAB_ACCESS_TOKEN'
gitlab_access_token = os.environ.get(gitlab_access_token_envvar_name)
global_visibility = 'public'
gitlab_hostname = 'gitlab.opencode.de'
gitlab_base_url = 'https://'+gitlab_hostname
temp_checkout_dir = './temp'
temp_key_file = './temp_priv_push.key'
git_username = 'Open CoDE git pull mirror automation'
git_useremail = 'git+bmi-souveraener-arbeitsplatz-tooling-gitlab-pull-mirror-1708-29pr0g9pj4or9yi6wfly6pbhg-issue@opencode.de'
main_config_file = 'mirror_automation_main.json'
group_config_file = 'mirror_automation_groups.json'

# Ensure project token is available
if not gitlab_access_token:
    sys.exit(f"! Please set environment variable {gitlab_access_token_envvar_name} with your gitlab access token for {gitlab_base_url}")

# Function: Load (json) configuration files and parse json
def load_config(file):
    logging.info(f"Loading {file}")
    if not os.path.isfile(file):
        sys.exit(f"! Cannot find mandatory json config file {file}")
    with open(file, 'r') as f:
        config = json.load(f)
    logging.info(f"Loaded {len(config)} config entries from {file}")
    return config

logging.info("Loading config files")
main_config = load_config(main_config_file)
group_config = load_config(group_config_file)

gl = gitlab.Gitlab(url=gitlab_base_url, private_token=gitlab_access_token)

# Function: Do CLI calls
def cli_runner(cli_command):
    rc = os.system(cli_command)
    if rc > 0:
        sys.exit(f"! CLI command failed with Return code {rc}: {cli_command}")

# Function: Recursively create groups for a given group_path and return namespace object
def check_and_create_group_structure(group_path, namespace = None):
    group_stack = group_path.split('/')
    logging.info(f"Group hierarchy: {group_stack}")
    try:
        namespace = gl.namespaces.get(group_path)
        return namespace
    except gitlab.exceptions.GitlabGetError as e:
        logging.info(f"Group autocreate: group not found {group_path} - depth: {len(group_stack)}")
        if len(group_stack) == 1:
            sys.exit(f"Root group {group_path} must exists, please create it manually. If it already exists the provided access_token might not be authorized to access the group.")
        my_group = group_stack[-1]
        my_group_base_stack = group_stack[ : -1]
        my_group_base = '/'.join(my_group_base_stack)
        base_namespace = check_and_create_group_structure(my_group_base, namespace)
        logging.info(f"Creating {my_group} in {my_group_base}")
        parent_group = gl.groups.get(base_namespace.id)
        gl.groups.create({'name': my_group, 'path': my_group, 'parent_id': parent_group.id});
        namespace = gl.namespaces.get(group_path)
        return namespace

# Function: Implement the optional project extensions
# Currently supported:
# - delete_dirs: Remove defined directories recursively from repo
# - flatten_repo: If repo is modified or out of other reasons we want to flatten it
def process_extensions(repo, type, values):
    if type == "delete_dirs":
        for dir in values:
            dir = temp_checkout_dir+'/'+dir
            if os.path.exists(dir) and os.path.isdir(dir):
                logging.info(f"Exention {type}: Deleting {dir}")
                shutil.rmtree(dir)
                commit_changes(repo)
    elif type == "flatten_repo":
        logging.info(f"Exention {type}: Flattening repo")
        message = '\n'.join(values)
        cli_runner("cd "+temp_checkout_dir+"; git reset $(git commit-tree HEAD^\{tree} -m '"+message+"')")
    else:
        sys.exit(f"! Unknown or unsupported extension type: {type}")

# Function: Commit state of the repo
def commit_changes(repo):
    logging.info(f"Add & commit local changes")
    repo.git.add(A=True)
    repo.index.commit('Commit changes applied during OpenCoDE mirroring.')

# Process the group configuration
for grp_path, grp_cfg in group_config.items():
    logging.info(f"Processing group config for {grp_path}")
    namespace = check_and_create_group_structure(grp_path)
    group = gl.groups.get(namespace.id)
    if group.name != grp_cfg["name"] or group.description != grp_cfg["description"] or group.visibility != global_visibility:
        group.name = grp_cfg["name"]
        group.description = grp_cfg["description"]
        group.visibility = global_visibility
        group.save()

logging.info(f"Setting git global config 'user.*' and 'push.autoSetupRemote' to 'true'")
cli_runner('git config --global --add --bool push.autoSetupRemote true')
cli_runner(f"git config --global user.name '{git_username}'")
cli_runner(f"git config --global user.email '{git_useremail}'")

# process the projects / actual mirrors
for entity, config_file in main_config.items():
    entity_config = load_config(config_file)

    for cfg_name, cfg in entity_config.items():

        logging.info(f"Processing config for '{cfg_name}'")

        # ensure the checkout directory does not already exist
        if os.path.exists(temp_checkout_dir) and os.path.isdir(temp_checkout_dir):
            logging.info(f"Deleting already existing dir {temp_checkout_dir}")
            shutil.rmtree(temp_checkout_dir)

        # Ensure the group for the project exists
        namespace = check_and_create_group_structure(cfg["target_group"])

        group = gl.groups.get(namespace.id)
        projects = group.projects.list(get_all=True)
        project_id = None

        for project in projects:
            if project.path == cfg_name:
                project_id = project.id

        if not project_id:
            logging.info(f"Creating new project {cfg_name}")
            project = gl.projects.create({'name': cfg_name, 'path': cfg_name, 'namespace_id': namespace.id, 'initialize_with_readme': False})
        else:
            project = gl.projects.get(project_id)

        # Remove any branches protection settings from Gitlab mirror target
        prot_branches = project.protectedbranches.list(get_all=True)
        for prot_branch in prot_branches:
            prot_branch.delete()

        if "name" in cfg:
            project.name = cfg["name"]

        project.visibility = global_visibility
        # Hardcoded project configuration
        project.analytics_access_level = 'disabled'
        project.auto_devops_enabled = False
        project.builds_access_level = 'disabled'
        project.can_create_merge_request_in = False
        project.container_registry_access_level = 'disabled'
        project.container_registry_enabled = False
        project.description = f"Git pull mirror from upstream repo {cfg['source_url']}. Please use upstream repo for contributions."
        project.emails_enabled = False
        project.environments_access_level = 'disabled'
        project.feature_flags_access_level = 'disabled'
        project.forking_access_level = 'disabled'
        project.infrastructure_access_level = 'disabled'
        project.issues_access_level = 'disabled'
        project.issues_enabled = False
        project.jobs_enabled = False
        project.lfs_enabled = False
        project.merge_requests_access_level = 'disabled'
        project.merge_requests_enabled = False
        project.model_experiments_access_level = 'disabled'
        project.monitor_access_level = 'disabled'
        project.package_registry_access_level = 'disabled'
        project.packages_enabled = False
        project.pages_access_level = 'disabled'
        project.releases_access_level = 'disabled'
        project.repository_access_level = 'enabled'
        project.request_access_enabled = False
        project.requirements_enabled = False
        project.security_and_compliance_access_level = 'disabled'
        project.security_and_compliance_enabled = False
        project.service_desk_enabled = False
        project.snippets_access_level = 'disabled'
        project.snippets_enabled = False
        project.wiki_access_level = 'disabled'
        project.wiki_enabled = False
        project.save()
        logging.info(f"Updated project settings for {cfg_name}")

        key = rsa.generate_private_key(
            backend=crypto_default_backend(),
            public_exponent=65537,
            key_size=2048
        )

        private_key = key.private_bytes(
            crypto_serialization.Encoding.PEM,
            crypto_serialization.PrivateFormat.OpenSSH,
            crypto_serialization.NoEncryption()
        )

        public_key = key.public_key().public_bytes(
            crypto_serialization.Encoding.OpenSSH,
            crypto_serialization.PublicFormat.OpenSSH
        )

        project.keys.create({'title': 'Script managed deployment key', 'key': str(public_key, encoding='utf-8'), 'can_push': True})

        logging.info(f"Writing temporary private key to {temp_key_file}")

        with open(temp_key_file, "w") as key_file:
            key_file.write(str(private_key, encoding='utf-8'))
        os.chmod(temp_key_file, 0o400)

        logging.info(f"Cloning repo from {cfg['source_url']}")
        if "branch" in cfg:
            Repo.clone_from(cfg["source_url"], temp_checkout_dir, branch=cfg["branch"])
        else:
            Repo.clone_from(cfg["source_url"], temp_checkout_dir)
        repo = Repo(temp_checkout_dir)

        new_origin_uri = f"ssh://git@{gitlab_hostname}/{cfg['target_group']}/{cfg_name}.git"
        logging.info(f"Delete remote 'orgin' and set new origin to {new_origin_uri}")
        repo.delete_remote(repo.remotes["origin"])
        new_origin = repo.create_remote("origin", new_origin_uri)

        if "extensions" in cfg:
            for ext_type, ext_values in cfg["extensions"].items():
                process_extensions(repo, ext_type, ext_values)

        retry_counter = 1
        retry_max = 4
        while retry_counter < retry_max:
            logging.info(f"Pushing to new remote - attempt {retry_counter}")
            ssh_cmd = f"ssh -i ../{temp_key_file} -o StrictHostKeyChecking=no"
            try:
                with repo.git.custom_environment(GIT_SSH_COMMAND=ssh_cmd):
                    new_origin.push(force=True)
                retry_counter = retry_max
            except Exception as e:
                logging.error(e)
                retry_counter += 1

        logging.info(f"Deleting (all) project keys")
        keys = project.keys.list()
        for key in keys:
            project.keys.delete(key.id)
